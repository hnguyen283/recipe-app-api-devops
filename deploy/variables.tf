variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "awshungnd40@gmail.com"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

## START: FOR ECR

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "447958103462.dkr.ecr.ap-southeast-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "447958103462.dkr.ecr.ap-southeast-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

## END: FOR ECR

